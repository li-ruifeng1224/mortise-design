export const menuList:Array<MenuOptions> = [
  { 
    title: "设计指南", path: "/home",
    children: [
      { title: "简介", path: "/home/index"},
      { title: "设计价值观", path: "/home/index2"},
      { title: "设计原则", path: "/home/index3"},
      { title: "样式指南", path: "/home/index4"},
    ]
  },
  { 
    title: "开发指南", path: "/home2",
    children: [
      { title: "快速开始", path: "/home/index11"},
      { title: "目录结构", path: "/home/index12"},
      { title: "定制主题", path: "/home/index13"},
      { title: "设计变量", path: "/home/index14"},
      { title: "国际化", path: "/home/index24"},
    ]
  },
  { 
    title: "最佳实践", path: "/home3",
    children: [
      { title: "布局", path: "/home/index22"},
      { title: "路由和菜单", path: "/home/index23"},
      { title: "仪表盘", path: "/home/index34"},
      { title: "表单", path: "/home/index44"},
      { title: "列表页", path: "/home/index45"},
      { title: "单证详情页", path: "/home/index46"},
      { title: "打包构建", path: "/home/index48"},
    ]
  },
  { 
    title: "组件", path: "/components",
    children: [
      { title: "单选框", path: "/components/radio"},
      { title: "icon", path: "/home/index222"},
      { title: "upload", path: "/home/index3333"},
      { title: "image", path: "/home/index4444"},
      { title: "table", path: "/home/index44444"},
    ]
  },
]



interface MenuOptions {
  path: string;
  title: string;
  children?: MenuOptions[];
}
