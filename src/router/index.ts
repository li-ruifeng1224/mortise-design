import { createRouter, createWebHistory, RouteRecordRaw, } from "vue-router";

// * 导入所有自定义router
const metaRouters = import.meta.globEager("./modules/*.ts");


const routerArray: RouteRecordRaw[] = [];
// 处理导入的router
Object.keys(metaRouters).forEach(item => {
	// @ts-ignore
	Object.keys(metaRouters[item] as string).forEach((key: string) => {
	// @ts-ignore
		routerArray.push(...metaRouters[item][key]);
	});
});

console.log(routerArray);

const routes: RouteRecordRaw[] = [
	{
		path: "/",
		redirect: { name: "radio" }
	},
	...routerArray,
	{
		// 找不到路由重定向到404页面
		path: "/:pathMatch(.*)",
		redirect: { name: "404" }
	}
];


const router = createRouter({
	history: createWebHistory(),
	routes,
	strict: false,
	// 切换页面，滚动到最顶部
	scrollBehavior: () => ({ left: 0, top: 0 })
});


export default router;
