import { RouteRecordRaw } from "vue-router";
import { Layout } from "@/router/constant";


// 首页模块
const homeRouter: Array<RouteRecordRaw> = [
	{
		path: "/home",
		component: Layout,
		children: [
			{
				path: "",
				name: "home",
				component: () => import("@/views/home/index.vue"),
				meta: {
					title: "首页",
					key: "home"
				}
			}
		]
	}
];

export default homeRouter;
