import { RouteRecordRaw } from "vue-router";
import { Layout } from "@/router/constant";


// 组件模块
const componentsRouter: Array<RouteRecordRaw> = [
	{
		path: "/components",
		component: Layout,
    redirect: '/components/radio',
		children: [
			{
				path: "/components/radio",
				name: "radio",
				component: () => import("@/views/components/radio/index.vue"),
				meta: {
					title: "单选框",
					key: "radio"
				}
			}
		]
	}
];

export default componentsRouter;
