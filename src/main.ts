import { createApp } from 'vue'
import './style.css'
import 'element-plus/theme-chalk/dark/css-vars.css'

import router from "./router/index"
import 'highlight.js/styles/atelier-sulphurpool-dark.css' // 代码高亮的样式
import App from './App.vue'
import '@/styles/cover-old.scss'
import 'element-plus/dist/index.css'
import directives from "@/directive/index";
const app = createApp(App)
app.use(router).use(directives).mount('#app')
