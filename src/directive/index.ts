import { App } from "vue";
import copy from "./modules/copy";
import trim from "./modules/trim";
import highlight from './modules/highlight'


const directivesList: any = {
	copy,
	trim,
  highlight
};

const directives = {
	install: function (app: App<Element>) {
		Object.keys(directivesList).forEach(key => {
			// 注册自定义指令
			app.directive(key, directivesList[key]);
		});
	}
};

export default directives;
