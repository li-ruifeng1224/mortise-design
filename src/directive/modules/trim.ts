import type { Directive, DirectiveBinding } from 'vue';
import { ElMessage } from 'element-plus';

const trim: Directive = {
  updated (el) {
    // // 获取真实的inputDOM元素
    let input:any = el.children[0].children[0]
    try {
    // 如果不是el输入框 那么直接返回
      if (!input) {
        return false
      }
      // 获取当前光标所在位置
      const inputSelection = input.selectionStart
      // 获取加上空格后的字符串长度
      const valueLength = input.value.length
      let value = input.value
      // 这里是去除空格，可以通过其他的正则去做其他的操作(格式化手机号啊身份证号码之类的，后续也可以通过指令传入拓展成各种格式化指令)
      let newValue = value.replace(/\s/g, '')
      if (value !== newValue) {
        input.value = newValue
        if (input.value.length !== valueLength) {
          input.selectionStart = inputSelection - 1
          input.selectionEnd = inputSelection - 1
        }
        input.dispatchEvent(new Event('input'))
      }
    } catch (e) {
      console.log(e)
    }
  }
}
export default trim

