





import hljs from 'highlight.js' //导入代码高亮文件


import type { Directive, DirectiveBinding } from 'vue';
import { ElMessage } from 'element-plus';

const highlight: Directive =  function (el) {
    const blocks = el.querySelectorAll('pre');
    blocks.forEach((block: any) => {
        hljs.highlightBlock(block)
    })
  }

export default highlight
